# timestamp_parsing

# Build the program
gcc -O2 perf_push.c

# Run the program
./a.out data.txt

On running the program, it will create a target file `output.txt` in the same directory

# Benchmark

2 seconds for 300k records

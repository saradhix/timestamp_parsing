#python PerfPush.py <filename> <number of times data to push into server>
#python PerfPush.py t_25.hexa 2

#Each record 40 array position to 57 array position holds date and time in hexa, below is the sample
#Year Mn Dy Hr Mi Se Milli
#07e7 04 0b 10 1c 2b 02c4
#11-4-2023 16:28:43.708


import time, sys,  socket
from queue import Empty, Queue
from queue import Empty, Queue
from threading import Thread
from datetime import datetime, timedelta

rec_sent=0
stop_client=False
next_watermark = None

#Convert the hexa form of date and time to '%d-%m-%Y %H:%M:%S' format
def get_date_time(hexa_rec):
    print(f"line={hexa_rec}")
    print("{} {}".format(hexa_rec[40:44], int(hexa_rec[40:44], 16)))
    print("{} {}".format(hexa_rec[46:48], int(hexa_rec[46:48], 16)))
    print("From 46::{}".format(hexa_rec[46:]))
    tm_str = f"{int(hexa_rec[46:48], 16)}-{int(hexa_rec[44:46], 16)}-"\
        f"{int(hexa_rec[40:44], 16)} {int(hexa_rec[48:50], 16)}:"\
        f"{int(hexa_rec[50:52], 16)}:{int(hexa_rec[52:54], 16)}."\
        f"{int(hexa_rec[54:58], 16)}"

    print("+++ {}".format(tm_str))
    
    tm_obj = datetime.strptime(tm_str, '%d-%m-%Y %H:%M:%S.%f')
    return tm_obj


def update_timestamp(hexa_list):

    global next_watermark

    #Get the first record timestamp and last record time stamp and 
    #get the time diff in minutes
    #Add this time diff to every record, which moves the record by 1 step to new time
    sz = len(hexa_list)
    start_tm = get_date_time(hexa_list[0]) #Get the timestamp of first record
    end_tm   = get_date_time(hexa_list[sz - 1]) #Get the timestamp of last record
      
    #If previous last time is available use the same, else take last record time
    wtrmrk_tm = next_watermark
    if wtrmrk_tm != None:
        end_tm = wtrmrk_tm

    seconds_diff = (end_tm - start_tm).total_seconds()
    print(f"line49 - Time diff in seconds {seconds_diff} - first record time stamp {start_tm} "\
        f"and the last record time stamp {end_tm}")
    
    new_start_tm = start_tm + timedelta(seconds=(seconds_diff+1))
    print(f"The new first tm after adding time diff is : {new_start_tm}")
    
    new_tm_obj = None
    for i in range(0, sz):
        #Get the current record date and time and add the diff time
        ml_sec = hexa_list[i][54:58]
        rec_tm =  get_date_time(hexa_list[i])
        new_tm_obj = rec_tm + timedelta(seconds=(seconds_diff))

        #Convert new time to hexa
        msecs = int(hexa_list[i][54:58], 16)
        new_tm_hex = new_tm_obj.year.to_bytes(2, 'big').hex() + \
            new_tm_obj.month.to_bytes(1, 'big').hex() + \
            new_tm_obj.day.to_bytes(1, 'big').hex() + \
            new_tm_obj.hour.to_bytes(1, 'big').hex() + \
            new_tm_obj.minute.to_bytes(1, 'big').hex() + \
            new_tm_obj.second.to_bytes(1, 'big').hex() + \
            ml_sec

        print(f"line72 - {i}#{hexa_list[i][40:58]}#{rec_tm}.{msecs}#{new_tm_hex}#{new_tm_obj}.{msecs}")
        
        hexa_list[i] = hexa_list[i][:40] + new_tm_hex + hexa_list[i][58:]

    next_watermark = new_tm_obj + timedelta(seconds=5)
    return hexa_list


def send_data(hexa_data, host, port):
    send_data = hexa_data
    if isinstance(hexa_data, list):
        print(f"Sending {len(hexa_data)} records to GMF {host} and {port}")
        send_data = "".join(hexa_data)
    ss1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss1.connect((host, port))
    ss1.sendall(bytearray.fromhex(send_data))
    ss1.close()
    print(f"Data sent")


def producer(queue, upd_data, num):
    global  rec_sent

    for i in range(1, num+1):
        new_list = update_timestamp(upd_data)
        print(f'Inserting item {i} into the queue')
        queue.put(new_list)
        upd_data = new_list

    print(f'Producer done')



def consumer(queue):
    global rec_sent, stop_client
    i=1
    while (not stop_client):
        try:
            item = queue.get()
        except Empty:
            continue
        else:
            print(f'Processing item {i}')
            i = i + 1
            rec_sent = rec_sent + len(item)
#            send_data(item, "127.0.0.1", 8081)
            queue.task_done()


def main(data, rep):
    global rec_sent, stop_client
    queue = Queue()


    # create a producer thread and start it
    producer_thread = Thread(
        target=producer,
        args=(queue, data, rep,)
    )
    producer_thread.start()

    # create a consumer thread and start it
    consumer_thread = Thread(
        target=consumer,
        args=(queue,),
        daemon=True
    )
    consumer_thread.start()

    # wait for all tasks to be added to the queue
    producer_thread.join()

    # wait for all tasks on the queue to be completed
    queue.join()

    print(f"Sent {rec_sent} records, sleeping 2 seconds before final closure")
    time.sleep(2)
    stop_client=True


if __name__ == '__main__':

    print(f"Processing the hexa file {sys.argv[1]}")
    with open(sys.argv[1], "r") as hfile:
        upd_data = hfile.read().splitlines()

    repeat = int((sys.argv[2].lower()).strip())

    print(f"Repeating the processed data push for {repeat} times ")
    main(upd_data, repeat)
    print(f"Completed repeat push ")    


#include<stdio.h>
#include<time.h>
#include<string.h>
#include<stdlib.h>

#define MAX_LINE_LENGTH 5000
#define MAX_NUM_LINES 300000
#define MAX_FILE_SIZE MAX_LINE_LENGTH*MAX_NUM_LINES+1
#define MAX_TS_LENGTH 18

void get_date_time(const char *line, time_t *t, int *milli_seconds);
void  update_time_in_line(char *line, time_t timestamp, int milli_seconds);
void send_data(char *sz, int nbytes);
char server_data[MAX_FILE_SIZE+1] = {0};

int main(int argc, char *argv[])
{
    FILE *fp=NULL;
    char *filename = argv[1];
    char line[MAX_LINE_LENGTH+1] = {0};
    time_t new_ts = {0};
    int new_milli_second = 0;
    int line_size = 0;
    int server_data_idx = 0;
    char *lines[MAX_NUM_LINES] = {0};
    char *cur_line = NULL;

    int i = 0, cx = 0;
    time_t *ts = malloc(MAX_NUM_LINES*sizeof(time_t));
    int time_diff= 0, milli_diff = 0;
    int *millis = malloc(MAX_NUM_LINES*sizeof(int));
    if (ts == NULL || millis == NULL)
    {
        printf("Memory allocation failed\n");
        exit(1);
    }
    if (argc != 2)
    {
        printf("Please specify the filename as argument\n");
        exit(1);
    }

    fp = fopen(filename, "r");
    if (fp==NULL)
    {
        printf("Unable to open file %s\n", argv[1]);
        exit(1);
    }
    cx = 0;
    while(fgets(line, MAX_LINE_LENGTH, fp))
    {
        lines[cx] = strdup(line);
        get_date_time(line, &ts[cx], &millis[cx]);
        //printf("Line %d ts=%ld oldtime=%s", cx, ts[cx], asctime(localtime(&ts[cx]))); 
        cx++;
    }
    //printf("First timestamp=%s ms=%d ts=%ld\n", asctime(localtime(&ts[0])), millis[0], ts[0]);
    //printf("Last timestamp=%s ms=%d ts=%ld\n", asctime(localtime(&ts[cx-1])), millis[cx-1], ts[cx-1]);
    time_diff = ts[cx-1] - ts[0];
    milli_diff = millis[cx-1] - millis[0];
    //printf("time_diff = %d milli_diff = %d\n", time_diff, milli_diff);
    fclose(fp);
    server_data_idx = 0;
    for(i = 0; i < cx; i++)
    {
        cur_line = lines[i];
        //compute the new timestamp and the millisecond by adding the difference to the current timestamp
        new_ts = ts[i] + time_diff;
        new_milli_second = millis[i] + milli_diff;
        //update the line with the new timestamp
        update_time_in_line(cur_line, new_ts, new_milli_second);
        line_size = strlen(cur_line);
        memcpy(server_data + server_data_idx, line, line_size);
        server_data_idx += line_size;
        if (i % 100000 == 99999 || i+1==cx )
        {
            send_data(server_data, server_data_idx);
            server_data_idx = 0;
        }
        free(cur_line); //This will free the memory allocated by strdup
    }
    free(ts);
    free(millis);
}


void get_date_time(const char *line, time_t *timestamp, int *milli_seconds)
{
    int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
    struct tm t={0};
    time_t ts;
    //printf("Start_buffer=%s\n", line+40);

    sscanf(line+40, "%4x%2x%2x%2x%2x%2x%4x", &tm_year, &tm_mon, &tm_mday, &tm_hour, &tm_min, &tm_sec, milli_seconds);
    t.tm_year = tm_year - 1900;
    t.tm_mon = tm_mon - 1;
    t.tm_mday = tm_mday;
    t.tm_hour = tm_hour;
    t.tm_min = tm_min;
    t.tm_sec = tm_sec;
    t.tm_isdst = -1;
    //printf("In get_date_time::%s", asctime(&t));
    //printf("Milli seconds=%d\n", *milli_seconds);
    ts = mktime(&t);
    *timestamp = ts;
}

void  update_time_in_line(char *line, time_t timestamp, int milli_seconds)
{
    char buffer[MAX_TS_LENGTH+1] = {0};
    struct tm t, *ptm;
    ptm  =localtime(&timestamp);
    //memcpy(&t, ptm, sizeof(struct tm));  // Uncomment for debug
    time_t ts={0};
    int milli = 0;
    int tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec;
    //printf("UTL:Entered with ts=%ld newtime=%s", timestamp, asctime(localtime(&timestamp)));  //Uncomment for debug
    tm_year = ptm->tm_year + 1900;
    tm_mon = ptm->tm_mon + 1;
    tm_mday = ptm->tm_mday;
    tm_hour = ptm->tm_hour;
    tm_min = ptm->tm_min;
    tm_sec = ptm->tm_sec;
    sprintf(buffer, "%04x%02x%02x%02x%02x%02x%04x", tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, milli_seconds);
    //Copy the first 18 bytes of buffer to the line from 40th index
    //printf("line+40 before copy : %s\n", line+40);
    memcpy(line + 40, buffer, MAX_TS_LENGTH);
    //printf("line+40 after copy : %s\n", line+40);
    //get_date_time(line, &ts, &milli);
    //printf("parsed from updated = %s %d\n", asctime(localtime(&ts)), milli);
}

void send_data(char *sz, int nbytes)
{
    printf("Sending %d bytes to the server \n", nbytes);
}
